function fetchSubscriptions() {
    fetch('https://travel.techneapp.com/api/newsletter-subscription/public?siteCode=whv', {
      method: 'POST',
      headers: {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/111.0',
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'en-US,en;q=0.5',
        'Accept-Encoding': 'gzip, deflate, br',
        'Content-Type': 'application/json',
        'Referer': 'https://www.worldholidayvibes.com/',
        'Origin': 'https://www.worldholidayvibes.com',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'cross-site',
        'Sec-GPC': '1',
        'Connection': 'keep-alive',
        'TE': 'trailers'
      },
      body: JSON.stringify({
        Email: 'rolisi5800@huvacliq.com',
        FirstName: 'Rolisi',
        LastName: 'Roll',
        mobile: '44132456789',
        Status: true,
        otp: '947628',
        siteCode: 'whv'
      })
    })
    .then(response => response.json())
    .then(data => console.log(data))
    .catch(error => console.error(error));
  }